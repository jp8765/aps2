
public class Izziv2 {

	public static void main(String[] args) {
		//priprava tabele
		System.out.println("|   n     |    linearno  |   dvojisko   |");
		System.out.println("|---------+--------------+--------------|");
		for(int n = 1000; n<100000; n= n+1000){
		
			
			int [] tabela = generateTable(n);
	
			
			
			// funkciji
			long casL = 0;
			long casB = 0;
			
			for(int i = 0; i< 1000; i+= 1){
				
			int stevilo = (int) (Math.random() * n);
			casL +=  timeLinear(tabela, stevilo);
			casB +=  timeBinary(tabela, stevilo);
			
			//System.out.println(i + "       " + linear+ "       "+binary +"          " + stevilo);
			//System.out.printf("%9s|%",(i));
			
			
			}
			long linear = casL / 1000;
			long binary = casB / 1000;
			System.out.printf("|%9s|%14s|%14s|\n",n, linear, binary);
		}

	}
	
	


	public static long timeBinary(int[] n, int st) {
		long startTime = System.nanoTime();
		// iskanje elementa
		int mesto = findBinary(n,0, n.length-1, st);
		long executionTime = System.nanoTime() - startTime;
		return executionTime;
	}
	
	
	
	public static long timeLinear(int[] n, int st) {
		long startTime = System.nanoTime();
		// iskanje elementa
		int mesto = findLinear(n, st);
		long executionTime = System.nanoTime() - startTime;
		return executionTime;
	}
	
	static int[] generateTable(int n){
		int [] tabela = new int [n];
		for (int i = 0; i< n; i++) tabela[i] = i;
		return tabela; 
	}
	
	static int findLinear(int[] a, int v){
		for(int i = 0; i< a.length; i++){
			if(a[i] == v){
				return i;
			}
		}
		return -1; 
		
	}
	
	public static int findBinary(int [] tabela,int  left,int  right,int element) {
		if(left>right){
    		
    		return -1; 
    	}
    	int center = (left + right) / 2;
    	/*for(int j = left; j<= right; j++){
			if(j == center ) System.out.print("|"+tabela[j]+ "| ");
			else if (j == tabela.length-1)System.out.print(tabela[j]);
			else{
			System.out.print(tabela[j]+ " ");
			}
		}
    	System.out.println();
    	*/
    	if (tabela[center] == element){
    		//System.out.println("Found");
    		return center;
    	}
    	
    	else if (tabela[center] > element){
              return findBinary(tabela, left, center-1, element);
        	}
    	else if (tabela[center] < element )
             return findBinary(tabela, center+1, right, element) ;
    
    	else{
    		//System.out.println("Not Found");
    		return -1; 
    	}
	}
}
