import java.util.Scanner;
public class Naloga1 {
	static int [] count = new int [6];
	static int tmpCount = 0; 
	public static void main (String[]args){
				
	/*  moznost testnega preverjanja
	 * 	
	 	
		String[] argumenti = new String[4]; 
		argumenti[0] = "trace";
		argumenti[1] = "ms";
		argumenti[2] = "up";
		argumenti[3] = "9";
		args = argumenti;
		*/
	
		
		int tabela[] = new int[Integer.parseInt(args[3])];
        Scanner sc = new Scanner(System.in);
        
        for(int i = 0; i<tabela.length; i++){
		tabela[i] = sc.nextInt();
        }
        
		switch (args[1]){
			case "bs":
				buble(tabela, args);
				break; 
			case "ss":
				selection(tabela, args);
				break;
			case "is":
				insertion(tabela, args);
				break; 
			case "hs":
				heap(tabela, args);
				break;
			case "qs":
				quick(tabela, args);
				break;
			case "ms": 
				merge(tabela, args);
				break; 
			default: 
				System.out.println("error: switch("+args[2]+")");
		}
		
	}

	private static int [] quick(int[] tabela, String[] args) {
		if (tabela.length <=1){
			return tabela; 
		}
		//System.out.println("------TABELA--------");
	    //if(args[0].equals("trace")) izpis(tabela, args, -1);	
		
		int left = 0; 
		int right = tabela.length-1;
		int pivot = (left + right)/2;
		
		
		int i = 0;  
		int j = tabela.length-1;
		while (i <= j && args[2].equals("up")) {
		    while (tabela[i] < tabela[pivot] ) i = i + 1;
		    while (tabela[j] > tabela[pivot] ) j = j - 1;
		    if (i <= j ){
		        swap(tabela, i, j);
		        i = i + 1; 
		        j = j - 1;
		        }
		    
		   			    
		}
		while (i <= j && args[2].equals("down")) {
		    while (tabela[i] > tabela[pivot]) i = i + 1;
		    while (tabela[j] < tabela[pivot]) j = j - 1;
		    if (i <= j ){
		        swap(tabela, i, j);
		        i = i + 1; 
		        j = j - 1;
		        }
		   			    
		}
		i--;
		j++;
		if(args[0].equals("trace")) izpisQS(tabela, i,j);	
		int k = j;
		if (j == i) k--; 
		if (k < 0) k=0;
	   
		int [] tabL = new int [k];
	    int [] tabD = new int [tabela.length-k];
	    
	    
	    
	   // System.out.println(i+" "+j);
	    //System.out.println("TABELA LEVO");
	    if (tabela.length>3){
	    	for(int n = 0; n<k ;n++){
		    	tabL[n] = tabela[n];
		    //	System.out.print(tabL[n]);
		    }
	    	tabL = quick( tabL, args); 
	    	//System.out.println("TABELA desno");
	    	for(int n = j; n<tabela.length ;n++){
	    		//System.out.print(tabela[n]);
		    	tabD[n-j] =tabela [n];
		   
		    }
	    	
	    	tabD = quick( tabD, args); 
	    }
	    
	return tabela;
	}
	
	public static void izpisQS (int [] tabela, int i, int j ){
		
		for (int n = 0; n< tabela.length; n++){
			if (n == j) System.out.print("| ");
			if (n == i+1) System.out.print("| ");
			System.out.print(tabela[n]+ " ");
			
			
		}System.out.println();
		if(i == tabela.length)System.out.println("| ");
	}
	private static int [] merge(int[] tabela, String[] args) {
		int len = tabela.length; 
		int levo = len/2;
		if(len%2 != 0 && len>1) levo++;
		
	
		izpis(tabela, args, levo);
		if (len == 2){
			if(args[2].equals("up") && tabela[0] > tabela[1])
				swap(tabela, 0,1);
			
				
			
			else if(args[2].equals("down") && tabela[0] < tabela[1])
				swap(tabela,0 ,1);
			
			izpis(tabela, args, -1);
		}
		else if(len>1){
			int [] tabL = new int[levo];
			int [] tabD = new int[len-levo];
			
			for(int i = 0; i< levo; i++)
				tabL[i] = tabela[i];
			for(int i = levo; i<len; i++)
				tabD[i-levo] = tabela[i];
			
			tabL = merge(tabL, args);
			tabD = merge(tabD, args);
			
			tabela = mergeCombined(tabL, tabD, args);
			izpis(tabela, args, -1);
		}
		
		return tabela;
	}

	
	private static void buble(int[] tabela, String[] args) {
		for (int i = 0; i < tabela.length-1; i++){
			if (args[0].equals("trace"))izpis(tabela, args,i);
			for(int j = tabela.length-1; j> i; j--){
				if(tabela[j] < tabela[j-1]  && args[2].equals("up")){
					int tmp = tabela[j]; 
					tabela[j]= tabela[j-1];
					tabela[j-1] = tmp;
				}
				
				else if(tabela[j] > tabela[j-1]  && args[2].equals("down")){
					int tmp = tabela[j]; 
					tabela[j]= tabela[j-1];
					tabela[j-1] = tmp;
				}
				 
				
			}
			
		}
		if (args[0].equals("trace"))izpis(tabela, args,tabela.length-1);
		
	}
	
	private static void heap(int[] tabela, String[] args) {
		
		if (args[2].equals("up")){
			for(int i = tabela.length/2; i>0; i--){
	        	pogrezniHEAPup(tabela, i-1, tabela.length);   	
	        }
			if (args[0].equals("trace"))izpis(tabela, args,0);
			while (tabela.length > 0){
		        tabela = urediHEAPup(tabela);
		        if (args[0].equals("trace"))izpis(tabela, args,0);
		        }
			if (args[0].equals("count")){
				System.out.println(count[0] + " " + count[1]);
				System.out.println(count[2] + " " + count[3]);
				System.out.println(count[4] + " " + count[5]);
			}
		}
		else if(args[2].equals("down")){
			for(int i = tabela.length/2; i>0; i--){
	        	pogrezniHEAPdown(tabela, i-1, tabela.length);   	
	        }
			if (args[0].equals("trace"))izpis(tabela, args,0);
			while (tabela.length > 0){
		        tabela = urediHEAPdown(tabela);
		        if (args[0].equals("trace"))izpis(tabela, args,0);
		        }
			if (args[0].equals("count")){
				System.out.println(count[0] + " " + count[1]);
				System.out.println(count[2] + " " + count[3]);
				System.out.println(count[4] + " " + count[5]);
			}
			
		}
			
	}

	private static void insertion(int[] tabela, String[] args) {
		
		for (int i = 0; i<tabela.length; i++){
		
			
			int trenuten=tabela[ i]; 
			int j = i-1;
			count[0]++;
			if(args[2].equals("up"))
				
				while (j>= 0 && tabela[j]> trenuten){
					count[0]++;
					count[1]+=2;
					tabela[j+1] = tabela[j];
					j--;
					
				}
			else if(args[2].equals("down"))
				while (j>= 0 && tabela[j]< trenuten){
					count[0]++;
					tabela[j+1] = tabela[j];
					j--;
					
					count[0]++;
					count[1]+=2;
				}
			
			tabela[j+1] = trenuten;
			count[1]++;
			if(args[0].equals("trace"))izpis(tabela, args, i);
			
		}
		if (args[0].equals("count"))countizpis(tabela,args);
		
		
	}
	
	

 	private static void selection(int[] tabela, String[] args) {
		if(args[2].equals("up")){
			for(int i = 0; i< tabela.length; i++){
				int minIndex = i; 
				if(args[0].equals("trace"))izpis(tabela, args, i);
				for(int j = i; j<tabela.length; j++){
					
					if (tabela[j] < tabela[minIndex]){
						minIndex = j;
					}
					count[tmpCount]++;
					
				}
				count[tmpCount]+=i;
				if (minIndex != i){
					int tmp = tabela[i];
					tabela[i] = tabela[minIndex];
					tabela[minIndex] = tmp;
					
				}
				
			}
		}
		else if(args[2].equals("down")){
			for(int i = 0; i< tabela.length; i++){
				int maxIndex = i; 
				if(args[0].equals("trace"))izpis(tabela, args, i);
				for(int j = i; j<tabela.length; j++){
					
					if (tabela[j] > tabela[maxIndex]){
						maxIndex = j;
					}
					
				}
				count[0]+=i;
				if (maxIndex != i){
					int tmp = tabela[i];
					tabela[i] = tabela[maxIndex];
					tabela[maxIndex] = tmp;
					
				}
				
			}
		}
		
		if (args[0].equals("count")){
			System.out.println(count[0] + " " + count[0]);
			System.out.println(count[0] + " " + count[0]);
			System.out.println(count[0] + " " + count[0]);
		}
			
	}

	
	
	
	
	
	
	public static void swap(int [] tabela, int a, int b){
		int tmp = tabela[a];
		tabela[a] = tabela[b];
		tabela[b] = tmp; 
	}
	
	private static void countizpis( int  []tabela, String [] args){
		System.out.println(count[0] + " " + count[1]);
		System.out.println(count[2] + " " + count[3]);
		System.out.println(count[4] + " " + count[5]);
		
	}
	 
		public static void izpis (int a[], String [] argumenti, int num){
			int tmp = 0; 
			int j = 1; 
		
	    	for(int i = 0; i< a.length; i++){
	    		 if((argumenti[1].equals("ss") || (argumenti[1].equals("bs")) || (argumenti[1].equals("ms")) ) && i== num){
		    			System.out.print("| ");
		    		}
	    		 else if(argumenti[1].equals("is") && i-1== num){
	    			 
		    			System.out.print("| ");
		    			
		    		}
	    		 
	    		System.out.print(a[i] + " ");
	    		if (i == tmp  && argumenti[1].equals("hs")){
	    			if ( i != a.length-1) System.out.print("|");
	    			tmp += (int) Math.pow(2, j);
	    			j++;
	    		}
	    		
	    	}
	    	if(argumenti[1].equals("is") && num == a.length-1)System.out.print("| ");
	    	System.out.println();
	    }
		private static int[] mergeCombined(int[] tabL, int[] tabD, String []args) {
			int lenL = tabL.length;
			int lenD = tabD.length;
			/*		Zgolj pomozni izpis tabL in tabD
			 * 
			 * 
			 * System.out.println("---------------------------");
			 *		for (int i= 0;i<tabL.length;i++){
			 *			System.out.print(tabL[i]+ " ");
			 *		}System.out.println();
			 *		for (int i= 0;i<tabD.length;i++){
			 *			System.out.print(tabD[i]+ " ");
			 *		}System.out.println();
			 *		System.out.println("---------------------------");
			 */
			
			int [] tabela = new int [lenL+ lenD];
			
			int L = 0;
			int D = 0; 
			int i = 0;
			while(L<lenL && D<lenD){
				if((tabL[L]< tabD[D] && args[2].equals("up")) || 
				   (tabL[L]> tabD[D] && args[2].equals("down"))){
					tabela[i] = tabL[L];
					i++;
					L++;
				}
				else{
					tabela[i] = tabD[D];
					i++;
					D++;
				}
				
			}
			if (L != lenL)
				for(int j = i; j<tabela.length; j++){
					tabela[j] = tabL[L];
					L++;
				}
				
			
			else 
				for(int j = i; j<tabela.length; j++){
					tabela[j] = tabD[D];
					D++;
				}
			
			return tabela;
		}

		public static int [] urediHEAPup(int[] a) {
	    	int [] nova = new int[a.length -1];
	    	a[0] = a[a.length -1]; 
	    	count[1] +=3;
	    	for (int i = 0; i< nova.length;i++)
	    		nova[i] = a[i];
	    	for(int i = nova.length/2; i>0; i--){
	        	pogrezniHEAPup(nova, i-1, nova.length);
	        	
	        	
	        }
	    	
			return nova;
	    	
			
		}
	    static void pogrezniHEAPup(int a[], int i, int dolzKopice){
	    	int d = 2*i +2;
	    	int l = 2*i +1;
	    	
	    	//System.out.println(" stevilo je " +a[i]);
	    	
	    	if(dolzKopice > d){
	    		if( a[d] > a[l]){
	    			if (a[i] < a[2*i+2] ){
			    		//System.out.println("dal");
			    		int tmp = a[2*i +2]; 
			    		a[2*i+2] = a[i];
			    		a[i] = tmp; 
			    		
			    		pogrezniHEAPup(a, 2*i+2, dolzKopice);
			    		count[1] += 3;
			    	}count[0] ++;
	    			
	    		
	    		}
	    		else {
	    			if (a[i] < a[2*i+1]){
			    		//System.out.println("dad");
			    		int tmp = a[2*i+1];
			    		a[2*i+1] = a[i];
			    		a[i] = tmp; 
			    		pogrezniHEAPup(a, 2*i+1, dolzKopice);
			    		count[1] += 3;
			    	}count[0] ++;
	    		}
	    		
	    		
	    	}
	    	else if (dolzKopice > l){
	    		if (a[i] < a[2*i+1]){
		    		//System.out.println("dad");
		    		int tmp = a[2*i+1];
		    		a[2*i+1] = a[i];
		    		a[i] = tmp; 
		    		pogrezniHEAPup(a, 2*i+1, dolzKopice);
		    		count[1] += 3;
	    		}
	    		count[0] ++;
	    	}
	    	
	    	
	    	
	    	
	    }
	    static void pogrezniHEAPdown(int a[], int i, int dolzKopice){
	    	int d = 2*i +2;
	    	int l = 2*i +1;
	    	
	    	//System.out.println(" stevilo je " +a[i]);
	    	
	    	if(dolzKopice > d){
	    		if( a[d] < a[l]){
	    			if (a[i] > a[2*i+2] ){
			    		//System.out.println("dal");
			    		int tmp = a[2*i +2]; 
			    		a[2*i+2] = a[i];
			    		a[i] = tmp; 
			    		
			    		pogrezniHEAPdown(a, 2*i+2, dolzKopice);
			    	}
	    		
	    		}
	    		else {
	    			if (a[i] > a[2*i+1]){
			    		//System.out.println("dad");
			    		int tmp = a[2*i+1];
			    		a[2*i+1] = a[i];
			    		a[i] = tmp; 
			    		pogrezniHEAPdown(a, 2*i+1, dolzKopice);
			    	}
	    		}
	    	}
	    	else if (dolzKopice > l){
	    		if (a[i] > a[2*i+1]){
		    		//System.out.println("dad");
		    		int tmp = a[2*i+1];
		    		a[2*i+1] = a[i];
		    		a[i] = tmp; 
		    		pogrezniHEAPdown(a, 2*i+1, dolzKopice);
		    	}
	    	}
	    	
	    	
	    	
	    	
	    }
	    public static int [] urediHEAPdown(int[] a) {
	    	int [] nova = new int[a.length -1];
	    	a[0] = a[a.length -1]; 
	    	for (int i = 0; i< nova.length;i++)
	    		nova[i] = a[i];
	    	for(int i = nova.length/2; i>0; i--){
	        	pogrezniHEAPdown(nova, i-1, nova.length);
	        	
	        	
	        }
	    	
			return nova;
		}
}
