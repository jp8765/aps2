import java.util.Scanner;
public class Naloga1 {
	static int [] count = new int [6];
	static int tmpCount = 0; 
	public static void main (String[]args){
				
	/*  moznost testnega preverjanja
	 * 
	 *   moznosti za argumente
	 *   	argumenti[0] = "trace" / "count"
	 *   	argumenti[1] = "hs" / "bs" / "is" / "ss" / "bs" / "ms" 
	 *  	argumenti[2] = "up" / "down   
	 *  	argumenti[3] = To je stevilo znakov, ki jih zelite urejati
	 
		
		
	
	
		
		String[] argumenti = new String[4]; 
		argumenti[0] = "trace";
		argumenti[1] = "is";
		argumenti[2] = "up";
		argumenti[3] = "5";
		args = argumenti;
		*/
	
		
		int tabela[] = new int[Integer.parseInt(args[3])];
        Scanner sc = new Scanner(System.in);
        
        for(int i = 0; i<tabela.length; i++){
		tabela[i] = sc.nextInt();
        }
        sc.close();
		switch (args[1]){
			case "bs":
				buble(tabela, args);
				break; 
			case "ss":
				selection(tabela, args);
				break;
			case "is":
				insertion(tabela, args);
				break; 
			case "hs":
				heap(tabela, args);
				break;
			case "qs":
				quick(tabela, args);
				break;
			case "ms": 
				merge(tabela, args);
				break; 
			default: 
				System.out.println("error: switch("+args[2]+")");
		}
		
	}

	private static int [] quick(int[] tabela, String[] args) {
		if (tabela.length <=1){
			return tabela; 
		}
		
		int left = 0; 
		int right = tabela.length-1;
		int pivot = (left + right)/2;
		
		
		int i = 0;  
		int j = tabela.length-1;
		while (i <= j && args[2].equals("up")) {
		    while (tabela[i] < tabela[pivot] ) i = i + 1;
		    while (tabela[j] > tabela[pivot] ) j = j - 1;
		    if (i <= j ){
		        swap(tabela, i, j);
		        i = i + 1; 
		        j = j - 1;
		        }
		    
		   			    
		}
		while (i <= j && args[2].equals("down")) {
		    while (tabela[i] > tabela[pivot]) i = i + 1;
		    while (tabela[j] < tabela[pivot]) j = j - 1;
		    if (i <= j ){
		        swap(tabela, i, j);
		        i = i + 1; 
		        j = j - 1;
		        }
		   			    
		}
		i--;
		j++;
		if(args[0].equals("trace")) izpisQS(tabela, i,j);	
		int k = j;
		if (j == i) k--; 
		if (k < 0) k=0;
	   
		int [] tabL = new int [k];
	    int [] tabD = new int [tabela.length-k];
	    
	    
	  
	    if (tabela.length>3){
	    	for(int n = 0; n<k ;n++){
		    	tabL[n] = tabela[n];
		   
		    }
	    	tabL = quick( tabL, args); 	    	
	    	for(int n = j; n<tabela.length ;n++){	    		
		    	tabD[n-j] =tabela [n];		   
		    }	    	
	    	tabD = quick( tabD, args); 
	    }
	    
	return tabela;
	}
	
	
	private static int [] merge(int[] tabela, String[] args) {
		int len = tabela.length; 
		int levo = len/2;
		if(len%2 != 0 && len>1) levo++;
		
		
		if (len>1)izpis(tabela, args, levo);
		
		if (len == 2){
			if(args[2].equals("up") && tabela[0] > tabela[1])
				swap(tabela, 0,1);
			
				
			
			else if(args[2].equals("down") && tabela[0] < tabela[1])
				swap(tabela,0 ,1);
			
			izpis(tabela, args, -1);
		}
		else if(len>1){
			int [] tabL = new int[levo];
			int [] tabD = new int[len-levo];
			
			for(int i = 0; i< levo; i++)
				tabL[i] = tabela[i];
			for(int i = levo; i<len; i++)
				tabD[i-levo] = tabela[i];
			
			tabL = merge(tabL, args);
			tabD = merge(tabD, args);
			
			tabela = mergeCombine(tabL, tabD, args);
			izpis(tabela, args, -1);
		}
		
		return tabela;
	}

	
	private static void buble(int[] tabela, String[] args) {
		for (int i = 0; i < tabela.length-1; i++){
			if (args[0].equals("trace"))izpis(tabela, args,i);
			for(int j = tabela.length-1; j> i; j--){
				if(tabela[j] < tabela[j-1]  && args[2].equals("up")){
					swap(tabela, j, j-1);
				}
				
				else if(tabela[j] > tabela[j-1]  && args[2].equals("down")){
					swap(tabela, j, j-1);
				}
			}
			
		}
		if (args[0].equals("trace"))izpis(tabela, args,tabela.length-1);
		
	}
	
	private static void heap(int[] tabela, String[] args) {
		
	
		for(int i = tabela.length/2; i>0; i--){
			
				pogrezniHEAP(tabela, i-1, tabela.length, args);   	
		
        }
		if (args[0].equals("trace"))izpis(tabela, args,0);
		while (tabela.length > 0){
	        tabela = urediHEAP(tabela, args);
	        if (args[0].equals("trace"))izpis(tabela, args,0);
	        }
		
	}

	private static void insertion(int[] tabela, String[] args) {
		
		for (int i = 0; i<tabela.length; i++){
		
			
			int trenuten = tabela[i]; 
			int j = i-1;
			if(args[2].equals("up"))
				
				while (j>= 0 && tabela[j]> trenuten){
					tabela[j+1] = tabela[j];
					j--;
					
				}
			else if(args[2].equals("down"))
				while (j>= 0 && tabela[j]< trenuten){
					tabela[j+1] = tabela[j];
					j--;
				}
			
			tabela[j+1] = trenuten;
			if(args[0].equals("trace"))izpis(tabela, args, i);
			
		}
	//	if (args[0].equals("count"))countizpis(tabela,args);
		
		
	}
	
 	private static void selection(int[] tabela, String[] args) {
		
			for(int i = 0; i< tabela.length; i++){
				int minIndex = i; 
				if(args[0].equals("trace"))izpis(tabela, args, i);
				for(int j = i; j<tabela.length; j++){
					
					if (tabela[j] < tabela[minIndex] && args[2].equals("up")){
						minIndex = j;
					}
					else if (tabela[j] > tabela[minIndex] && args[2].equals("up")){
						minIndex = j;
					}
				}
				if (minIndex != i){
					swap(tabela, i, minIndex);
				}
				
			}
			
	}

	
	public static void swap(int [] tabela, int a, int b){
		int tmp = tabela[a];
		tabela[a] = tabela[b];
		tabela[b] = tmp; 
	}
	public static void izpisQS (int [] tabela, int i, int j ){
		
		for (int n = 0; n< tabela.length; n++){
			if (n == j) System.out.print("| ");
			if (n == i+1) System.out.print("| ");
			System.out.print(tabela[n]+ " ");
			
			
		}System.out.println();
		if(i == tabela.length)System.out.println("| ");
	}
	
	
	/*private static void countizpis( int  []tabela, String [] args){
		System.out.println(count[0] + " " + count[1]);
		System.out.println(count[2] + " " + count[3]);
		System.out.println(count[4] + " " + count[5]);
		
	}
	 */
	
	
		public static void izpis (int a[], String [] argumenti, int num){
			int tmp = 0; 
			int j = 1; 
		
	    	for(int i = 0; i< a.length; i++){
	    		 if((argumenti[1].equals("ss") || (argumenti[1].equals("bs")) || (argumenti[1].equals("ms")) ) && i== num){
		    			System.out.print("| ");
		    		}
	    		 else if(argumenti[1].equals("is") && i-1== num){
	    			 
		    			System.out.print("| ");
		    			
		    		}
	    		 
	    		System.out.print(a[i] + " ");
	    		if (i == tmp  && argumenti[1].equals("hs")){
	    			if ( i != a.length-1) System.out.print("|");
	    			tmp += (int) Math.pow(2, j);
	    			j++;
	    		}
	    		
	    	}
	    	if(argumenti[1].equals("is") && num == a.length-1)System.out.print("| ");
	    	System.out.println();
	    }
		
		private static int[] mergeCombine(int[] tabL, int[] tabD, String []args) {
			int lenL = tabL.length;
			int lenD = tabD.length;
			/*		pomozni izpis tabL in tabD
			 * 
			 * 
			 * System.out.println("---------------------------");
			 *		for (int i= 0;i<tabL.length;i++){
			 *			System.out.print(tabL[i]+ " ");
			 *		}System.out.println();
			 *		for (int i= 0;i<tabD.length;i++){
			 *			System.out.print(tabD[i]+ " ");
			 *		}System.out.println();
			 *		System.out.println("---------------------------");
			 */
			
			int [] tabela = new int [lenL+ lenD];
			
			int L = 0;
			int D = 0; 
			int i = 0;
			while(L<lenL && D<lenD){
				if((tabL[L]< tabD[D] && args[2].equals("up")) || 
				   (tabL[L]> tabD[D] && args[2].equals("down"))){
					tabela[i] = tabL[L];
					i++;
					L++;
				}
				else{
					tabela[i] = tabD[D];
					i++;
					D++;
				}
				
			}
			if (L != lenL)
				for(int j = i; j<tabela.length; j++){
					tabela[j] = tabL[L];
					L++;
				}
				
			
			else 
				for(int j = i; j<tabela.length; j++){
					tabela[j] = tabD[D];
					D++;
				}
			
			return tabela;
		}
		
	    static void pogrezniHEAP(int tabela[], int i, int dolzKopice, String [] args){
	    	int d = 2*i +2;
	    	int l = 2*i +1;   	
	    	if(args[2].equals("up")){
		    	if(dolzKopice > d){
		    		if( tabela[d] > tabela[l]){
		    			if (tabela[i] < tabela[2*i+2] ){
				    		swap(tabela, 2*i+2, i);
				    		
				    		pogrezniHEAP(tabela, 2*i+2, dolzKopice, args);
				    	}
		    		}
		    		else {
		    			if (tabela[i] < tabela[2*i+1]){
		    				swap(tabela, 2*i+1, i);
		    				pogrezniHEAP(tabela, 2*i+1, dolzKopice, args);
				    	}
		    		}
		    	}
		    	else if (dolzKopice > l){
		    		if (tabela[i] < tabela[2*i+1]){
			    		int tmp = tabela[2*i+1];
			    		tabela[2*i+1] = tabela[i];
			    		tabela[i] = tmp; 
			    		pogrezniHEAP(tabela, 2*i+1, dolzKopice, args);
			    		
		    		}
		    	}
	    	}
		    else if(args[2].equals("down")){
		    	if(dolzKopice > d){
		    		if( tabela[d] < tabela[l]){
		    			if (tabela[i] > tabela[2*i+2] ){
				    		//System.out.println("dal");
				    		int tmp = tabela[2*i +2]; 
				    		tabela[2*i+2] = tabela[i];
				    		tabela[i] = tmp; 
				    		
				    		pogrezniHEAP(tabela, 2*i+2, dolzKopice, args);
				    	}
		    		
		    		}
		    		else {
		    			if (tabela[i] > tabela[2*i+1]){
				    		//System.out.println("dad");
				    		int tmp = tabela[2*i+1];
				    		tabela[2*i+1] = tabela[i];
				    		tabela[i] = tmp; 
				    		pogrezniHEAP(tabela, 2*i+1, dolzKopice, args);
				    	}
		    		}
		    	}
		    	else if (dolzKopice > l){
		    		if (tabela[i] > tabela[2*i+1]){
			    		//System.out.println("dad");
			    		int tmp = tabela[2*i+1];
			    		tabela[2*i+1] = tabela[i];
			    		tabela[i] = tmp; 
			    		pogrezniHEAP(tabela, 2*i+1, dolzKopice, args);
			    	}
		    	}
		    }
	    }
	  
	    public static int [] urediHEAP(int[] tabela, String[] args) {
	    	int [] nova = new int[tabela.length -1];
	    	tabela[0] = tabela[tabela.length -1]; 
	    	for (int i = 0; i< nova.length;i++)
	    		nova[i] = tabela[i];
	    	for(int i = nova.length/2; i>0; i--){
	        	if(args[2].equals("up"))pogrezniHEAP(nova, i-1, nova.length, args);
	        	else if(args[2].equals("down"))pogrezniHEAP(nova, i-1, nova.length, args);
	        	
	        	
	        }
	    	
			return nova;
		}
}
